import path from "path";
import resolve from "@rollup/plugin-node-resolve";
import terser from "@rollup/plugin-terser";

import { Component } from "totates";
import data from "./component.json" assert { type: "json" };
const component = new Component(data);

const translations = {
    espr: "external:@totates/quotquot/v1:espression",
    fluent: "external:@totates/translation/v1",
    logging: "external:@totates/logging/v1",
    "trusted-types": "external:@totates/trusted-types/v1"
};

export default [
    {
        input: "espression.mjs",
        output: {
            file: path.join(component.destDir, "espression.mjs"),
            format: "esm"
        },
        plugins: [
            resolve({ browser: true }),
            component.resource("espression", "application/javascript"),
            process.env.BUILD === "production" && terser()
        ]
    },
    ...[
        "element",
        "plugins/class",
        "plugins/events",
        "plugins/fluent",
        "plugins/style",
        "plugins/template",
        "router"
    ].map(name => ({
        input: `${name}.mjs`,
        output: {
            file: path.join(component.destDir, `${name}.mjs`),
            format: "esm"
        },
        makeAbsoluteExternalsRelative: false,
        plugins: [
            resolve({ browser: true }),
            component.resolver(translations),
            component.resource(
                name.replace(/^plugins\//u, "plugin."), "application/javascript"
            ),
            process.env.BUILD === "production" && terser()
        ]
    })),
    ...component.getConfig()
];
