import logging from "external:@totates/logging/v1";
import { sort } from "external:@totates/blaze-css/v10";
import policy from "external:@totates/trusted-types/v1";
import { registerElementBundleResolver } from "external:@totates/translation/v1";
import { QuotquotElement } from "external:@totates/quotquot/v1:element";
import FluentRenderer from "external:@totates/quotquot/v1:plugin.fluent";
import StylePlugin from "external:@totates/quotquot/v1:plugin.style";

import html from "./index.html";
import index from "./index.json";

const logger = logging.getLogger("@totates/quotquot/v1:element.unknown-page");
// logger.setLevel("WARN");

const template = document.createElement("template");
template.innerHTML = policy.createHTML(html);

// TODO: use asset resolution
const styles = index.select["@totates/blaze-css/v10"];
sort(styles);

registerElementBundleResolver(index, import.meta.url);

class UnknownPageHTMLElement extends QuotquotElement {
    constructor() {
        const state = {
            styles: styles.map(name => `/static/@totates/blaze-css/v10/css/${name}.css`)
        };
        super({ template, state, Renderers: [FluentRenderer], Plugins: [StylePlugin] });
    }
}

window.customElements.define(index.tagName, UnknownPageHTMLElement);
